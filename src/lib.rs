pub mod message;
pub mod unit;

pub use self::unit::{State, Unit};

#[cfg(test)]
mod tests {
    // extern crate bincode;
    //
    // use message::*;
    // use unit::*;
    //
    // #[derive(Serialize, Deserialize, Debug)]
    // struct Dummy {}
    // impl Unit for Dummy {
    //     type Question = String;
    //     type Answer = ();
    //     type State = ();
    //     fn run(&self, _state: &mut (), _question: String) {}
    // }
    //
    // #[test]
    // fn serde_cleverness() {
    //     let sending = "Hello, world".to_string();
    //     let msg: FromServerOutbound<Dummy> = FromServerOutbound::Question(&sending);
    //     let ser = bincode::serialize(&msg, bincode::Infinite).unwrap();
    //     let des = bincode::deserialize::<FromServerInbound>(&ser).unwrap();
    //     let receiving = match des {
    //         FromServerInbound::Question(question) => question,
    //         _ => panic!(),
    //     };
    //
    //     println!("{:?}", bincode::deserialize::<String>(&receiving).unwrap());
    // }
}
