//! Defines the traits that compose the Unit API.

use serde::de::Deserialize;
use serde::ser::Serialize;
use std::fmt;

/// A trait for "things" that can be shared through the internet. It has no required methods and is
// implemented automatically for all types which can implement it.
pub trait Transmit: Serialize + for<'de> Deserialize<'de> + fmt::Debug + Send {}

// Implementation is trivial for all types:
impl<T: Serialize + for<'de> Deserialize<'de> + fmt::Debug + Send> Transmit for T {}

/// A trait to respresent mutable state. All mutable state must start at some initial state, so we
/// have to define it with a required method. Also, we need to maintain backup copies in order to
/// make state update **atomic**. That is very important in the client side. Therefore, all
/// `State`s are required to be `Clone`.
pub trait State: Clone {
    fn init() -> Self;
}

// When T implements Default, T implements State.
impl<T: Default + Clone> State for T {
    fn init() -> T {
        T::default()
    }
}

/// Defines which operations will run in the client once it is loaded. It receives a question and
/// must produce an answer while optionally updating an internal state, which starts at a definite
/// value as defined by the suitalbe implementation of `State::init`.
pub trait Unit: 'static + Sync + Transmit {
    type Question: 'static + Sync + Transmit;
    type Answer: 'static + Transmit;
    type State: 'static + Transmit + State;
    fn run(&self, state: &mut Self::State, input: Self::Question) -> Self::Answer;
}

pub trait UnitPrototype: 'static + Sync + Transmit {
    type Question: 'static + Sync + Transmit;
    type Answer: 'static + Transmit;
    type State: 'static + Transmit + State;
}

impl<P: UnitPrototype> Unit for P {
    type Question = P::Question;
    type Answer = P::Answer;
    type State = P::State;

    fn run(&self, _: &mut Self::State, _: Self::Question) -> Self::Answer {
        panic!("Unreachable: `UnitPrototype`s are not meant to be used as `Unit`s.")
    }
}
