use serde_derive::{Deserialize, Serialize};

use crate::Unit;

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub enum ExecType {
    Retrieving,
    Regenerating,
}

impl ExecType {
    pub fn decide<T>(self, t: T) -> Option<T> {
        match self {
            ExecType::Retrieving => Some(t),
            ExecType::Regenerating => None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum FromClient<U: Unit> {
    NotLoaded {
        oid: u64,
    },
    Loaded,
    LoadFailed {
        // see https://github.com/serde-rs/serde/issues/1296
        #[serde(deserialize_with = "Option::<U>::deserialize")]
        maybe_unit: Option<U>,
        maybe_state: Option<U::State>,
        message: String,
    },
    Answer {
        oid: u64,
        answer: U::Answer,
    },
    DecodeError {
        oid: u64,
        message: String,
    },
    Retrieve {
        // see https://github.com/serde-rs/serde/issues/1296
        #[serde(deserialize_with = "Option::<U>::deserialize")]
        maybe_unit: Option<U>,
        maybe_state: Option<U::State>,
    },
}

#[derive(Serialize, Deserialize, Debug)]
pub enum FromServer<U: Unit> {
    Unit {
        // see https://github.com/serde-rs/serde/issues/1296
        #[serde(deserialize_with = "U::deserialize")]
        unit: U,
        maybe_state: Option<U::State>,
        exec_type: ExecType,
    },
    Question {
        oid: u64,
        question: U::Question,
    },
    DecodeError {
        message: String,
    },
    Retrieve,
    End,
}
